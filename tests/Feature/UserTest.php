<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class UserTest extends TestCase
{

    /**
     * @test
     */
    public function when_a_user_logs_in_they_are_redirected_to_the_home_page()
    {
        $this->signIn();
        $this->get('/')->assertRedirect('home');
    }

    /**
    * @test
    */
    public function users_can_register()
    {
        $this->post('register', [
            'email' => 'test@testing.com',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'password' => 'Password',
            'password_confirmation' => 'Password'
        ]);

        $this->assertDatabaseHas('users', [
            'email' => 'test@testing.com',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname'
        ]);
    }

    /**
    * @test
    */
    public function users_can_edit_thier_properties()
    {
        $originalAttributes = [
            'email' => 'test@testing.com',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
        ];
        $this->signIn(factory(User::class)->create($originalAttributes));

        $updatedAttributes = [
            'email' => 'changed@testing.com',
            'firstname' => 'Changed',
            'lastname' => 'Changed'
        ];
        $this->patch('user', $updatedAttributes);

        $this->assertDatabaseMissing('users', $originalAttributes);
        $this->assertDatabaseHas('users', $updatedAttributes);
    }

    /**
    * @test
    */
    public function guests_cannot_edit_user_properties()
    {
        $originalAttributes = [
            'email' => 'test@testing.com',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
        ];
        $this->signIn(factory(User::class)->create($originalAttributes));
        Auth::logout();
        $updatedAttributes = [
            'email' => 'changed@testing.com',
            'firstname' => 'Changed',
            'lastname' => 'Changed'
        ];
        $this->patch('user', $updatedAttributes)->assertRedirect('login');

        $this->assertDatabaseHas('users', $originalAttributes);
        $this->assertDatabaseMissing('users', $updatedAttributes);
    }

    /**
    * @test
    */
    public function unauthorized_users_cannot_edit_thier_properties()
    {
        $originalAttributes = [
            'email' => 'test@testing.com',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname'
        ];
        $maya = $this->signIn(factory(User::class)->create($originalAttributes));

        $this->signIn();

        $updatedAttributes = [
            'email' => 'changed@testing.com',
            'firstname' => 'Changed',
            'lastname' => 'Changed'
        ];
        $this->patch('user', $updatedAttributes);

        $this->assertDatabaseMissing('users', [
            'id' => $maya->id,
            'email' => 'changed@testing.com',
            'firstname' => 'Changed',
            'lastname' => 'Changed'
        ]);
        $this->assertDatabaseHas('users', [
            'id' => $maya->id,
            'email' => 'test@testing.com',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname'
        ]);
    }

    /**
    * @test
    */
    public function users_can_delete_their_accounts()
    {
        $user = $this->signIn();
        $this->delete('user');
        $this->assertDatabaseMissing('users', [
            'id' => $user->id
        ]);
    }

    /**
    * @test
    */
    public function guests_cannot_delete_users()
    {
        $user = $this->signIn();
        auth()->logout();

        $this->delete('user')->assertRedirect('login');

        $this->assertDatabaseHas('users', [
            'id' => $user->id
        ]);
    }

    /**
    * @test
    */
    public function unauthorized_users_cannot_delete_users()
    {
        $amy = $this->signIn();
        $this->signIn();

        $this->delete('user');
        
        $this->assertDatabaseHas('users', [
            'id' => $amy->id
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_upload_an_avatar()
    {
        $this->withoutExceptionHandling();
        $user = $this->signIn();
        Storage::fake('public');

        $this->post('avatar', [
            'avatar' => $file = UploadedFile::fake()->image('avatar.jpg')
        ]);

        $this->assertEquals(asset('avatars/'.$file->hashName()), $user->fresh()->avatar);
        Storage::disk('public')->assertExists('avatars/'.$file->hashName());
    }
}
