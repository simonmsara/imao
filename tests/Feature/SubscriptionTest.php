<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Subscription;
use App\Post;
use Illuminate\Support\Facades\Auth;
use App\Comment;

class SubscriptionTest extends TestCase
{
    /**
    * @test
    */
    public function a_user_is_automatically_subscribed_to_their_posts_comments_and_replies_on_creation()
    {
        $user = $this->signIn();
        $post = factory('App\Post')->create();
        $comment = factory('App\Comment')->create([
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);
        factory('App\Comment')->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);

        $this->assertCount(3, Subscription::where('user_id', $user->id)->get());
    }

    /**
    * @test
    */
    public function a_user_can_subscribe_to_a_post()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $munya = $this->signIn();
        $this->post('subscribe', [
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ],
        [
            'referer' => env('APP_URL').$post->path
        ]);

        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $munya->id,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_unsubscribe()
    {
        $user = $this->signIn();

        $post = factory(Post::class)->create();
        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $user->id,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);

        $this->post('unsubscribe', [
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\Post'
        ]);
        $this->assertDatabaseMissing('subscriptions', [
            'user_id' => $user->id,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);
    }

    /**
    * @test
    */
    public function a_guest_cannot_subscribe_to_an_object()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        Auth::logout();

        $this->post('subscribe', [
            'user_id' => 1,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ])->assertRedirect('login');

        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $user->id,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);
    }

    /**
    * @test
    */
    public function a_guest_cannot_unsubscribe_a_user()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        Auth::logout();

        $this->post('unsubscribe', [
            'user_id' => 1,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ])->assertRedirect('login');

        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $user->id,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_unsubscribe_a_user()
    {
        $simon = $this->signIn();
        $post = factory(Post::class)->create();
        Auth::logout();

        $billy = $this->signIn();
        $this->post('unsubscribe', [
            'user_id' => $simon->id,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);

        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $simon->id,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_only_subscribe_to_exsting_objects()
    {
        $this->signIn();

        $this->post('subscribe', [
            'subscribable_id' => 1000000,
            'subscribable_type' => 'App\\Post'
        ]);
        $this->assertDatabaseMissing('subscriptions', [
            'subscribable_id' => 1000000,
            'subscribable_type' => 'App\\Post'
        ]);

        $this->post('subscribe', [
            'subscribable_id' => 1000000,
            'subscribable_type' => 'App\\Comment'
        ]);
        $this->assertDatabaseMissing('subscriptions', [
            'subscribable_id' => 1000000,
            'subscribable_type' => 'App\\Comment'
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_only_subscribe_once_to_an_object()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $this->assertCount(1, Subscription::where('subscribable_id', $post->id)->get());

        $this->post('subscribe', [
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);
        $this->assertCount(1, Subscription::where('subscribable_id', $post->id)->get());

    }

    /**
    * @test
    */
    public function another_user_cannot_subscribe_to_a_reply()
    {
        $simon = $this->signIn();
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->create([
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);
        $reply = factory(Comment::class)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);
        Auth::logout();

        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $simon->id,
            'subscribable_id' => $reply->id,
            'subscribable_type' => 'App\\Comment'
        ]);

        $tom = $this->signIn();
        $this->post('subscribe', [
            'subscribable_id' => $reply->id,
            'subscribable_type' => 'App\\Comment'
        ]);

        $this->assertDatabaseMissing('subscriptions', [
            'user_id' => $tom->id,
            'subscribable_id' => $reply->id,
            'subscribable_type' => 'App\\Comment'
        ]);
    }

    /**
    * @test
    */
    public function a_user_must_be_on_the_parent_post_to_subscribe_to_an_object()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $kuda = $this->signIn(factory(User::class)->create());
        $this->post('subscribe', [
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ],
        [
            'referer' => env('APP_URL').'/post/5'
        ]);

        $this->assertDatabaseMissing('subscriptions', [
            'user_id' => $kuda->id,
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);
    }

    /**
    * @test
    */
    public function a_user_cannot_subscribe_to_replies_they_do_not_own()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $reply = factory(Comment::class)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);
        $this->signIn();
        $this->withHeaders([
            'HTTP_Referer' => env('APP_URL') .'/post/'. $comment->commentable_id
        ])->json('POST', 'subscribe', [
            'subscribable_id' => $reply->id,
            'subscribable_type' => 'App\\Comment'
        ])->assertStatus(303);
    }
}
