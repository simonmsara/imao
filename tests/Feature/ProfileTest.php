<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProfileTest extends TestCase
{
    /**
     * @test
     */
    public function a_profile_is_created_automatically_upon_user_registration()
    {
        $this->post('register', [
            'email' => $this->faker->email,
            'firstname' => $this->faker->firstname,
            'lastname' => $this->faker->lastname,
            'password' => 'Password',
            'password_confirmation' => 'Password'
        ]);

        $this->assertNotEquals(null, auth()->user()->profile);
    }

    /**
    * @test
    */
    public function a_user_can_edit_all_fields_of_their_profile()
    {
        $this->post('register', [
            'email' => $this->faker->email,
            'firstname' => $this->faker->firstname,
            'lastname' => $this->faker->lastname,
            'password' => 'Password',
            'password_confirmation' => 'Password'
        ]);

        $attrbutes = [
            'description' => $this->faker->paragraph,
            'occupation' => $this->faker->jobTitle,
            'dob' => $this->faker->date,
            'country' => $this->faker->country,
        ];

        $this->post('profile', $attrbutes);

        $this->assertDatabaseHas('profiles', $attrbutes);
    }

    /**
    * @test
    */
    public function a_guest_cannot_edit_a_profile()
    {
        $attrbutes = [
            'description' => $this->faker->paragraph,
            'occupation' => $this->faker->jobTitle,
            'dob' => $this->faker->date,
            'country' => $this->faker->country,
        ];

        $this->post('profile', $attrbutes)->assertRedirect('login');

        $this->assertDatabaseMissing('profiles', $attrbutes);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_update_a_profile()
    {
        $anna = $this->signIn();
        $this->signIn();

        $attrbutes = [
            'user_id' => $anna->id,
            'description' => $this->faker->paragraph,
            'occupation' => $this->faker->jobTitle,
            'dob' => $this->faker->date,
            'country' => $this->faker->country,
        ];

        $this->post('profile', $attrbutes);

        $this->assertDatabaseMissing('profiles', $attrbutes);
    }
}
