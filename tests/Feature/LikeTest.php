<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Post;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class LikeTest extends TestCase
{
    /**
    * @test
    */
    public function a_user_can_like_posts()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();

        $this->post('like', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post',
            'dislike' => false
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_dislike_posts()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();

        $this->post('dislike', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_unlike_posts()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();

        $this->post('like', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->post('unlike', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id,
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_undislike_posts()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();

        $this->post('dislike', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->post('unlike', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id,
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_like_comments()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->create([
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);

        $this->post('like', [
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => false
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_dislike_comments()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->create([
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);

        $this->post('dislike', [
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => true
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_unlike_comments()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->create([
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);

        $this->post('like', [
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        $this->post('unlike', [
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id,
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => false
        ]);
    }

    /**
    * @test
    */
    public function a_guest_cannot_like_posts()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        Auth::logout();

        $this->post('like', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ])->assertRedirect('login');

        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id,
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post',
            'dislike' => false
        ]);
    }

    /**
    * @test
    */
    public function a_guest_cannot_unlike_objects()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->create([
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);

        $this->post('like', [
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        Auth::logout();

        $this->post('unlike', [
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ])->assertRedirect('login');

        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => false
        ]);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_unlike_objects()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->create([
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);
        $this->post('like', [
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        Auth::logout();

        $this->signIn();
        $this->post('unlike', [
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);

        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => false
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_only_like_objects_that_exist()
    {
        $user = $this->signIn();

        $this->post('like', [
            'likeable_id' => 1000,
            'likeable_type' => 'App\\Post'
        ])->assertStatus(404);
        
        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id,
            'likeable_id' => 1000,
            'likeable_type' => 'App\\Post',
            'dislike' => false
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_like_a_post_only_once()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();

        $this->post('like', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->post('like', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);

        $this->assertCount(1, $user->likes);
    }
    

    /**
    * @test
    */
    public function a_user_can_dislike_a_post_only_once()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();

        $this->post('dislike', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->post('dislike', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);

        $this->assertCount(1, $user->dislikes);
    }

    /**
    * @test
    */
    public function a_user_can_only_like_or_dislike_not_both()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $this->post('like', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->post('dislike', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertDatabaseMissing('likes', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post',
            'dislike' => false
        ]);
        $this->assertDatabaseHas('likes', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);

        $this->post('like', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertDatabaseMissing('likes', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);
        $this->assertDatabaseHas('likes', [
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post',
            'dislike' => false
        ]);
    }
}
