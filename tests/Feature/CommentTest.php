<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use App\Comment;

class CommentTest extends TestCase
{
    /**
    * @test
    */
    public function a_user_can_create_a_comment()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $attributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $this->withHeaders([
            'HTTP_Referer' => env('APP_URL') . $post->path
        ])->json('POST', 'comment', $attributes);

        $this->assertDatabaseHas('comments', $attributes);
    }

    /**
    * @test
    */
    public function a_guest_cannot_create_a_comment()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        Auth::logout();

        $commentAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $this->post('comment', $commentAttributes)->assertRedirect('login');
        $this->assertDatabaseMissing('comments', $commentAttributes);
    }

    /**
    * @test
    */
    public function a_user_must_be_on_the_posts_page_to_save_a_comment()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $commentOne = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $this->withHeaders([
            'HTTP_Referer' => env('APP_URL') . $post->path
        ])->json('POST', 'comment', $commentOne);

        $this->assertDatabaseHas('comments', $commentOne);
        $commentTwo = [
            'body' => $this->faker->paragraph,
            'commentable_id' => Post::first()->id,
            'commentable_type' => 'App\\Post'
        ];
        $this->post('comment', $commentTwo, [
            'referer' => 'https://blog.test/post/'.factory(Post::class)->create()->id
        ]);
        $this->assertDatabaseMissing('comments', $commentTwo);
    }

    /**
    * @test
    */
    public function a_user_can_edit_their_comments()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $commentAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $comment = factory(Comment::class)->create($commentAttributes);
        $this->assertDatabaseHas('comments', $commentAttributes);

        $changed = [
            'body' => 'Changed',
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $this->patch('comment/'. $comment->id, $changed);
        $this->assertDatabaseHas('comments', $changed);
        $this->assertDatabaseMissing('comments', $commentAttributes);
    }

    /**
    * @test
    */
    public function a_guest_cannot_edit_a_comment()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $commentAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $comment = factory(Comment::class)->create($commentAttributes);
        $this->assertDatabaseHas('comments', $commentAttributes);
        
        Auth::logout();
        $changed = [
            'body' => 'Changed',
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $this->patch('comment/'. $comment->id, $changed)->assertRedirect('login');
        $this->assertDatabaseHas('comments', $commentAttributes);
        $this->assertDatabaseMissing('comments', $changed);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_edit_a_comment()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $commentAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $comment = factory(Comment::class)->create($commentAttributes);
        $this->assertDatabaseHas('comments', $commentAttributes);
        
        $this->signIn(factory(User::class)->create());

        $changed = [
            'body' => 'Changed',
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $this->patch('comment/'. $comment->id, $changed)->assertStatus(403);
        $this->assertDatabaseHas('comments', $commentAttributes);
        $this->assertDatabaseMissing('comments', $changed);
    }

    /**
    * @test
    */
    public function a_user_can_delete_their_comments()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $commentAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $comment = factory(Comment::class)->create($commentAttributes);
        $this->assertDatabaseHas('comments', $commentAttributes);
        $this->delete('comment/'.$comment->id);
        $this->assertDatabaseMissing('comments', $commentAttributes);
    }

    /**
    * @test
    */
    public function a_guest_cannot_delete_comments()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $commentAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        $comment = factory(Comment::class)->create($commentAttributes);
        $this->assertDatabaseHas('comments', $commentAttributes);
        Auth::logout();
        $this->delete('comment/'.$comment->id)->assertRedirect('login');
        $this->assertDatabaseHas('comments', $commentAttributes);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_delete_comments()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $commentAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        factory(Comment::class)->create($commentAttributes);
        $this->assertDatabaseHas('comments', $commentAttributes);

        $this->signIn(factory(User::class)->create());
        $this->delete('comment/'.Comment::first()->id);
        $this->assertDatabaseHas('comments', $commentAttributes);
    }

    /**
    * @test
    */
    public function a_user_can_reply_to_comments()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->create([
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);

        $replyAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ];
        $this->withHeaders([
            'HTTP_Referer' => env('APP_URL') . $post->path
        ])->json('POST', 'comment', $replyAttributes);

        $this->assertDatabaseHas('comments', $replyAttributes);
    }

    /**
    * @test
    */
    public function a_guest_cannot_reply_to_comments()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $commentAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ];
        factory(Comment::class)->create($commentAttributes);
        $this->assertDatabaseHas('comments', $commentAttributes);
        Auth::logout();
        $replyAttributes = [
            'body' => $this->faker->paragraph,
            'commentable_id' => Comment::first()->id,
            'commentable_type' => 'App\\Comment'
        ];
        $this->post('comment', $replyAttributes)->assertRedirect('login');
        $this->assertDatabaseMissing('comments', $replyAttributes);
    }

    /**
    * @test
    */
    public function a_user_can_only_comment_on_an_existing_object()
    {
        $this->signIn();

        $body = $this->faker->paragraph;

        $this->post('comment', [
            'body' => $body,
            'commentable_id' => 10000,
            'commentable_type' => 'App\\Post'
        ],
        [
            'HTTP_Referer' => "http://imao/post/10000"
        ]);
        
        $this->post('comment', [
            'body' => $body,
            'commentable_id' => 10000,
            'commentable_type' => 'App\\Comment'
        ],
        [
            'HTTP_Referer' => "http://imao/post/10000"
        ]);

        $this->assertDatabaseMissing('comments', [
            'body' => $body,
            'user_id' => auth()->id()
        ]);
    }
}
