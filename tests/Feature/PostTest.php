<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PostTest extends TestCase
{
    public function setUp(): void
    {
        parent::setup();

        $this->user = factory(User::class)->create();

        $this->postAttributes = [
            'title' => $this->faker->sentence,
            'abstract' => $this->faker->paragraph,
            'body' => $this->faker->paragraph
        ];

        $this->createPost = $this->actingAs($this->user)->post('post', $this->postAttributes);
    }

    /**
     * @test
     */
    public function a_signed_in_user_can_create_a_post()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        $this->assertDatabaseHas('posts', [
            'user_id' => $user->id,
            'title' => $post->title
        ]);
    }

    /**
     * @test
     */
    public function a_guest_cannot_create_a_post()
    {
        Auth::logout();
        $this->post('post', $this->postAttributes)->assertRedirect('login');
    }

    /**
    * @test
    */
    public function a_guest_can_view_any_post()
    {
        $this->withoutExceptionHandling();
        $post = factory(Post::class)->create([
            'published_at' => Carbon::now()->toDateTimeString()
        ]);
        Auth::logout();
        $this->get($post->path)->assertOK();
    }

    /**
    * @test
    */
    public function a_user_can_edit_their_post()
    {
        $this->createPost;
        $this->assertDatabaseHas('posts', $this->postAttributes);
        $editedAttributes = [
            'title' => 'Changed',
            'abstract' => 'Changed',
            'body' => 'Changed'
        ];
        $this->patch('post/'.Post::first()->id, $editedAttributes);
        $this->assertDatabaseHas('posts', $editedAttributes);
        $this->assertDatabaseMissing('posts',  $this->postAttributes);
    }

    /**
    * @test
    */
    public function a_guest_cannot_edit_a_post()
    {
        $this->createPost;
        $this->assertDatabaseHas('posts', $this->postAttributes);
        $editedAttributes = [
            'title' => 'Changed',
            'abstract' => 'Changed',
            'body' => 'Changed'
        ];
        Auth::logout();
        $this->patch('post/'.Post::first()->id, $editedAttributes)->assertRedirect('login');
        $this->assertDatabaseHas('posts', $this->postAttributes);
        $this->assertDatabaseMissing('posts', $editedAttributes);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_edit_a_post()
    {
        $this->createPost;
        $this->assertDatabaseHas('posts', $this->postAttributes);
        $editedAttributes = [
            'title' => 'Changed',
            'abstract' => 'Changed',
            'body' => 'Changed'
        ];
        $this->actingAs(factory(User::class)->create())
            ->patch('post/'.Post::first()->id, $editedAttributes);
        $this->assertDatabaseHas('posts', $this->postAttributes);
        $this->assertDatabaseMissing('posts', $editedAttributes);
    }

    /**
    * @test
    */
    public function a_user_can_delete_their_posts()
    {
        $this->createPost;
        $this->assertDatabaseHas('posts', $this->postAttributes);
        $this->delete('post/'.Post::first()->id);
        $this->assertDatabaseMissing('posts', $this->postAttributes);
    }

    /**
    * @test
    */
    public function a_guest_cannot_delete_posts()
    {
        $this->createPost;
        $this->assertDatabaseHas('posts', $this->postAttributes);
        Auth::logout();
        $this->delete('post/'.Post::first()->id)->assertRedirect('login');
        $this->assertDatabaseHas('posts', $this->postAttributes);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_delete_posts()
    {
        $this->createPost;
        $this->assertDatabaseHas('posts', $this->postAttributes);
        Auth::logout();
        $this->actingAs(factory(User::class)->create())
            ->delete('post/'.Post::first()->id);
        $this->assertDatabaseHas('posts', $this->postAttributes);
    }

    /**
    * @test
    */
    public function a_user_can_publish_their_posts()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $this->post('publish/'.$post->id);

        $this->assertNotEquals(null, $post->fresh()->published_at);
    }

    /**
    * @test
    */
    public function a_guest_cannot_publish_a_post()
    {
        Auth::logout();
        $post = factory(Post::class)->create();
        $this->post('publish/'.$post->id)->assertRedirect('login');
        $this->assertEquals(null, $post->published_at);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_publish_a_post()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $this->signIn();
        $this->post('publish/'.$post->id);

        $this->assertEquals(null, $post->published_at);
    }

    /**
    * @test
    */
    public function a_user_can_unpublish_their_posts()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $this->post('publish/'.$post->id);
        $this->get('unpublish/'.$post->id);

        $this->assertEquals(null, $post->fresh()->published_at);
    }

    /**
    * @test
    */
    public function a_guest_cannot_unpublish_a_post()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $this->post('publish/'.$post->id);
        Auth::logout();
        $this->get('unpublish/'.$post->id)->assertRedirect('login');

        $this->assertNotEquals(null, $post->fresh()->published_at);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_unpublish_a_post()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $this->post('publish/'.$post->id);
        $this->signIn();
        $this->get('unpublish/'.$post->id);
        $this->assertNotEquals(null, $post->fresh()->published_at);
    }

    /**
    * @test
    */
    public function a_user_can_set_a_specific_future_publish_date_and_time()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $futureDate = '2020-06-08 15:00:12';
        $this->post('publish/'.$post->id, [
            'datetime' => $futureDate
        ]);

        $this->assertEquals($futureDate, $post->fresh()->published_at);
    }

    /**
    * @test
    */
    public function a_custom_publish_at_date_must_be_a_valid_date()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $futureDate = 'Not A Date';
        $this->post('publish/'.$post->id, [
            'datetime' => $futureDate
        ]);

        $this->assertNotEquals($futureDate, $post->fresh()->published_at);
    }

    /**
    * @test
    */
    public function a_user_cannot_set_a_publish_date_in_the_past()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $futureDate = '2018-06-08 15:00:12';
        $this->post('publish/'.$post->id, [
            'datetime' => $futureDate
        ]);
        $this->assertNotEquals($futureDate, $post->fresh()->published_at);
        $this->assertNotEquals(null, $post->fresh()->published_at);
    }

    /**
    * @test
    */
    public function a_user_cannot_see_a_post_if_it_is_not_published()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $this->signIn();
        $this->get('post/'.$post->id)->assertStatus(404);
    }

    /**
    * @test
    */
    public function a_user_is_redirected_to_the_edit_page_if_the_post_is_not_published()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $this->get('post/'.$post->id)->assertRedirect('post/'.$post->id.'/edit');
    }
}
