<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use App\Follow;

class FollowTest extends TestCase
{
    /**
    * @test
    */
    public function a_user_can_follow_other_users()
    {
        $michael = $this->signIn();
        $john = $this->signIn();

        $this->get('follow/'.$michael->id);

        $this->assertDatabaseHas('follows', [
            'follower_id' => $john->id,
            'leader_id' => $michael->id
        ]);
    }


    /**
    * @test
    */
    public function a_user_cannot_follow_themself()
    {
        $this->signIn();
        $this->get('follow/'.auth()->id())->assertStatus(400);
        $this->assertDatabaseMissing('follows', [
            'follower_id' => auth()->id(),
            'leader_id' => auth()->id()
        ]);
    }

    /**
    * @test
    */
    public function a_guest_cannot_follow_users()
    {
        $user = $this->signIn();
        Auth::logout();
        $this->get('follow/'.$user->id)->assertRedirect('login');
        $this->assertDatabaseMissing('follows', [
            'leader_id' => $user->id
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_unfollow_their_leaders()
    {
        $marie = $this->signIn();
        Auth::logout();

        $this->signIn();
        $this->get('follow/'.$marie->id);
        $this->assertDatabaseHas('follows',[
            'follower_id' => auth()->id(),
            'leader_id' => $marie->id
        ]);

        $this->delete('follow/'.$marie->id);
        $this->assertDatabaseMissing('follows',[
            'follower_id' => auth()->id(),
            'leader_id' => $marie->id
        ]);
    }

    /**
    * @test
    */
    public function a_guest_cannot_unfollow()
    {
        $marie = $this->signIn();
        Auth::logout();

        $anna = $this->signIn();
        $this->get('follow/'.$marie->id);
        Auth::logout();

        $this->assertDatabaseHas('follows',[
            'follower_id' => $anna->id,
            'leader_id' => $marie->id
        ]);

        $this->delete('follow/'.$marie->id)->assertRedirect('login');

        $this->assertDatabaseHas('follows',[
            'follower_id' => $anna->id,
            'leader_id' => $marie->id
        ]);
    }

    /**
    * @test
    */
    public function an_unauthorized_user_cannot_unfollow_another_users_leaders()
    {
        $marie = $this->signIn();
        Auth::logout();

        $anna = $this->signIn();
        $this->get('follow/'.$marie->id);
        Auth::logout();

        $this->assertDatabaseHas('follows',[
            'follower_id' => $anna->id,
            'leader_id' => $marie->id
        ]);
        
        $this->signIn();
        $this->delete('follow/'.$marie->id);

        $this->assertDatabaseHas('follows',[
            'follower_id' => $anna->id,
            'leader_id' => $marie->id
        ]);
    }

    /**
    * @test
    */
    public function a_user_can_only_follow_users_that_exist()
    {
        $this->signIn();
        $this->get('follow/1000')->assertStatus(404);
    }

    /**
    * @test
    */
    public function a_user_can_only_unfollow_users_they_are_following()
    {
        $this->signIn();
        $this->delete('follow/1000')->assertStatus(404);
    }

    /**
    * @test
    */
    public function a_user_cannot_follow_a_leader_more_than_once()
    {
        $sam = $this->signIn();
        Auth::logout();

        $anna = $this->signIn();
        $this->get('follow/'.$sam->id);

        $this->assertDatabaseHas('follows', [
            'follower_id' => auth()->id(),
            'leader_id' => $sam->id
        ]);

        $this->get('follow/'.$sam->id)->assertStatus(400);
        $this->assertCount(1, $anna->leaders);
    }
}
