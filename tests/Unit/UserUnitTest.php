<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Profile;
use App\Follow;
use App\Post;
use App\Comment;
use App\Like;

class UserUnitTest extends TestCase
{
    /**
    * @test
    */
    public function a_user_has_a_profile()
    {
        $user = $this->signIn();
        factory(Profile::class)->create();
        $this->assertInstanceOf('App\Profile', $user->profile);
    }

    /**
    * @test
    */
    public function a_user_has_followers()
    {
        $marie = $this->signIn();
        $tom = $this->signIn();
        Follow::create([
            'follower_id' => $marie->id,
            'leader_id' => $tom->id
        ]);
        $this->assertInstanceOf('App\User', $tom->followers->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_followers()
    {
        $marie = $this->signIn();
        $tom = $this->signIn();
        $james = $this->signIn();
        $frank = $this->signIn();
        Follow::create([
            'follower_id' => $tom->id,
            'leader_id' => $marie->id
        ]);
        Follow::create([
            'follower_id' => $james->id,
            'leader_id' => $marie->id
        ]);
        Follow::create([
            'follower_id' => $frank->id,
            'leader_id' => $marie->id
        ]);
        $this->assertEquals(3, $marie->followers_count);
    }

    /**
    * @test
    */
    public function a_user_knows_if_the_authenticated_user_is_a_follower()
    {
        $tom = $this->signIn();
        $marie = $this->signIn();
        Follow::create([
            'follower_id' => $marie->id,
            'leader_id' => $tom->id
        ]);
        $this->assertTrue($tom->followed);

        $this->signIn();
        $this->assertFalse($tom->followed);
    }

    /**
    * @test
    */
    public function a_user_has_leaders()
    {
        $marie = $this->signIn();
        $tom = $this->signIn();
        Follow::create([
            'follower_id' => $tom->id,
            'leader_id' => $marie->id
        ]);
        $this->assertInstanceOf('App\User', $tom->leaders->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_leaders()
    {
        $marie = $this->signIn();
        $tom = $this->signIn();
        $james = $this->signIn();
        $frank = $this->signIn();
        Follow::create([
            'leader_id' => $tom->id,
            'follower_id' => $marie->id
        ]);
        Follow::create([
            'leader_id' => $james->id,
            'follower_id' => $marie->id
        ]);
        Follow::create([
            'leader_id' => $frank->id,
            'follower_id' => $marie->id
        ]);
        $this->assertEquals(3, $marie->leaders_count);
    }

    /**
    * @test
    */
    public function a_user_knows_if_the_authenticated_user_is_a_leader()
    {
        
        $marie = $this->signIn();
        $tom = $this->signIn();
        Follow::create([
            'follower_id' => $marie->id,
            'leader_id' => $tom->id
        ]);
        $this->assertTrue($marie->lead);

        $this->signIn();
        $this->assertFalse($marie->lead);
    }

    /**
    * @test
    */
    public function a_user_has_posts()
    {
        $user = $this->signIn();
        factory(Post::class)->create();

        $this->assertInstanceOf('App\Post', $user->posts()->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_posts()
    {
        $user = $this->signIn();
        factory(Post::class, 3)->create();

        $this->assertEquals(3, $user->posts_count);
    }

    /**
    * @test
    */
    public function a_user_has_comments()
    {
        $user = $this->signIn();
        factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertInstanceOf('App\Comment', $user->comments->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_comments()
    {
        $user = $this->signIn();
        factory(Comment::class, 3)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        $this->assertEquals(3, $user->comments_count);
    }

    /**
    * @test
    */
    public function a_user_has_replies()
    {
        $user = $this->signIn();
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        factory(Comment::class)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);

        $this->assertInstanceOf('App\Comment', $user->replies->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_replies()
    {
        $user = $this->signIn();
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        factory(Comment::class, 3)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);

        $this->assertEquals(3, $user->replies_count);
    }


    /**
    * @test
    */
    public function a_user_has_subscriptions()
    {
        $user = $this->signIn();
        factory(Post::class)->create();

        $this->assertInstanceOf('App\Subscription', $user->subscriptions->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_subscriptions()
    {
        $user = $this->signIn();
        factory(Comment::class, 3)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        $this->assertEquals(4, $user->subscriptions_count);
    }

    /**
    * @test
    */
    public function a_user_has_post_subscriptions()
    {
        $user = $this->signIn();
        factory(Post::class)->create();

        $this->assertInstanceOf('App\Subscription', $user->postSubscriptions->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_post_subscriptions()
    {
        $user = $this->signIn();
        factory(Comment::class, 3)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        $this->assertEquals(1, $user->post_subscriptions_count);
    }

    /**
    * @test
    */
    public function a_user_has_comment_subscriptions()
    {
        $user = $this->signIn();
        factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        $this->assertInstanceOf('App\Subscription', $user->commentSubscriptions->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_comment_subscriptions()
    {
        $user = $this->signIn();
        factory(Comment::class, 3)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        $this->assertEquals(3, $user->comment_subscriptions_count);
    }

    /**
    * @test
    */
    public function a_user_has_likes()
    {
        $user = $this->signIn();
        factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertInstanceOf('App\Like', $user->likes->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_likes()
    {
        $user = $this->signIn();
        factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post'
        ]);
        factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post'
        ]);
        factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post'
        ]);

        $this->assertEquals(3, $user->likes_count);
    }

    /**
    * @test
    */
    public function a_user_has_dislikes()
    {
        $user = $this->signIn();
        factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);
        $this->assertInstanceOf('App\Like', $user->dislikes->first());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_dislikes()
    {
        $user = $this->signIn();
        factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);
        factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);
        factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);

        $this->assertEquals(3, $user->dislikes_count);
    }

    /**
    * @test
    */
    public function a_user_can_concatenate_its_firstname_and_lastname()
    {
        $user = factory(User::class)->create([
            'firstname' => 'Simon',
            'lastname' => 'Msara'
        ]);

        $this->assertEquals('Simon Msara', $user->fullname);
    }

    /**
    * @test
    */
    public function a_user_knows_the_absolute_path_to_its_avatar()
    {
        $user = $this->signIn();
        $user->update([
            'avatar' => 'avatars/avatar_hash.jpg'
        ]);
        $this->assertEquals('http://imao.test/avatars/avatar_hash.jpg', $user->avatar);
    }

    /**
    * @test
    */
    public function a_user_has_a_default_avatar()
    {
        $user = $this->signIn();
        $this->assertEquals('http://imao.test/avatars/default.svg', $user->fresh()->avatar);
    }

    /**
    * @test
    */
    public function a_user_knows_if_its_posts_are_published_or_not()
    {
        $user = $this->signIn();
        $post = factory(Post::class)->create();
        $this->assertEquals(0, $user->fresh()->publishedPosts->count());
        $post->update(['published_at' => now()]);
        $this->assertEquals(1, $user->fresh()->publishedPosts->count());
    }

    /**
    * @test
    */
    public function a_user_can_count_its_published_posts()
    {
        $user = $this->signIn();
        factory(Post::class)->create([
            'published_at' => now(),
            'user_id' => $user->id
        ]);
        $this->assertEquals(1, $user->published_posts_count);
        $this->assertEquals(0, $user->unpublished_posts_count);
    }

    /**
    * @test
    */
    public function a_user_can_count_its_unpublished_posts()
    {
        $user = $this->signIn();
        factory(Post::class)->create([
            'user_id' => $user->id
        ]);
        $this->assertEquals(1, $user->unpublished_posts_count);
        $this->assertEquals(0, $user->published_posts_count);
    }

}
