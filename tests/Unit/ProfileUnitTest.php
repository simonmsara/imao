<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Profile;

class ProfileUnitTest extends TestCase
{
    /**
    * @test
    */
    public function a_profile_belongs_to_a_user()
    {
        $profile = factory(Profile::class)->create();
        $this->assertInstanceOf('App\User', $profile->user);
    }
}
