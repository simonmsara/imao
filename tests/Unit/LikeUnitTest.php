<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Like;
use App\Post;
use App\Comment;

class LikeUnitTest extends TestCase
{
    /**
    * @test
    */
    public function a_like_knows_its_likeable_parent()
    {
        $this->signIn();
        $like = factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertInstanceOf('App\Post', $like->likeable);
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $like = factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        $this->assertInstanceOf('App\Comment', $like->likeable);
    }

    /**
    * @test
    */
    public function a_like_belongs_to_a_user()
    {
        $this->signIn();
        $like = factory(Like::class)->create([
            'likeable_id' => factory(Post::class)->create(),
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertInstanceOf('App\User', $like->user);
    }
}
