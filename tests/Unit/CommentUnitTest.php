<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Comment;
use App\Post;
use App\Subscription;
use App\Like;

class CommentUnitTest extends TestCase
{
    /**
    * @test
    */
    public function a_comment_belongs_to_a_user()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertInstanceOf('App\User', $comment->user);
    }

    /**
    * @test
    */
    public function a_comment_knows_if_the_authenticated_user_is_the_owner()
    {
        $comment1 = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertFalse($comment1->owner);

        $this->signIn();
        $comment2 = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertTrue($comment2->owner);
    }

    /**
    * @test
    */
    public function a_comment_belongs_to_a_post()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertInstanceOf('App\Post', $comment->commentable);
    }

    /**
    * @test
    */
    public function a_reply_belongs_to_a_comment()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $reply = factory(Comment::class)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);
        $this->assertInstanceOf('App\Comment', $reply->commentable);
    }

    /**
    * @test
    */
    public function a_comment_has_replies()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        factory(Comment::class)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);
        $this->assertInstanceOf('App\Comment', $comment->replies[0]);
    }

    /**
    * @test
    */
    public function a_comment_can_count_its_replies()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        factory(Comment::class, 3)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);
        $this->assertEquals(3, $comment->replies_count);
    }

    /**
    * @test
    */
    public function a_comment_knows_if_the_authenticated_user_has_replied()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        factory(Comment::class)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);
        $this->assertFalse($comment->replied);

        $this->signIn();
        factory(Comment::class)->create([
            'commentable_id' => $comment->id,
            'commentable_type' => 'App\\Comment'
        ]);
        $this->assertTrue($comment->replied);
    }

    /**
    * @test
    */
    public function a_comment_has_subscriptions()
    {
        $this->signIn();
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        $this->assertInstanceOf('App\Subscription', $comment->subscriptions[0]);
    }

    /**
    * @test
    */
    public function a_comment_knows_if_the_authenticated_user_has_subscribed()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertFalse($comment->subscribed);
        $this->signIn();
        $this->assertFalse($comment->subscribed);

        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertTrue($comment->subscribed);
    }

    /**
    * @test
    */
    public function a_comment_knows_how_many_subscribers_it_has()
    {
        $this->signIn();
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        $this->signIn();
        factory(Subscription::class)->create([
            'subscribable_id' => $comment->id,
            'subscribable_type' => 'App\\Comment'
        ]);

        $this->assertEquals(2, $comment->subscriptions_count);
    }

    /**
    * @test
    */
    public function a_comment_has_likes()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        
        $this->assertInstanceOf('App\Like', $comment->likes[0]);
    }

    /**
    * @test
    */
    public function a_comment_can_count_its_likes()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);

        $this->signIn();
        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        
        $this->assertEquals(2, $comment->likes_count);
    }

    /**
    * @test
    */
    public function a_comment_knows_if_the_authenticated_user_has_liked()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertFalse($comment->liked);

        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        $this->assertFalse($comment->liked);
        $this->signIn();
        $this->assertFalse($comment->liked);

        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment'
        ]);
        $this->assertTrue($comment->liked);
    }

    /**
    * @test
    */
    public function a_comment_has_dislikes()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => true
        ]);
        
        $this->assertInstanceOf('App\Like', $comment->dislikes[0]);
    }

    /**
    * @test
    */
    public function a_comment_can_count_its_dislikes()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);

        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => true
        ]);

        $this->signIn();
        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => true
        ]);
        
        $this->assertEquals(2, $comment->dislikes_count);
    }

    /**
    * @test
    */
    public function a_comment_knows_if_the_authenticated_user_has_disliked()
    {
        $comment = factory(Comment::class)->create([
            'commentable_id' => factory(Post::class)->create(),
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertFalse($comment->disliked);

        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => true
        ]);
        $this->assertFalse($comment->disliked);
        $this->signIn();
        $this->assertFalse($comment->disliked);

        factory(Like::class)->create([
            'likeable_id' => $comment->id,
            'likeable_type' => 'App\\Comment',
            'dislike' => true
        ]);
        $this->assertTrue($comment->disliked);
    }
}
