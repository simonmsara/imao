<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Post;
use App\Like;
use App\Comment;
use App\Subscription;
use Illuminate\Support\Facades\Auth;
use App\Favourite;

class PostUnitTest extends TestCase
{
    /**
    * @test
    */
    public function a_post_belongs_to_a_user()
    {
        $post = factory(Post::class)->create();
        $this->assertInstanceOf('App\User', $post->user);
    }

    // /**
    // * @test
    // */
    // public function a_users_posts_are_deleted_when_a_user_is_deleted()
    // {
    //     $this->signIn();
    //     $post = factory(Post::class)->create();
    //     $this->delete('user');

    //     $this->assertDatabaseMissing('posts', [
    //         'id' => $post->id,
    //         'title' => $post->title
    //     ]);
    // }

    /**
    * @test
    */
    public function posts_have_a_path_property()
    {
        $post = factory(Post::class)->create();
        $this->assertEquals('/post/'.$post->id, $post->path);
    }

    /**
    * @test
    */
    public function posts_have_likes()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $post->likes()->create([
            'user_id' => auth()->id()
        ]);

        $this->assertInstanceOf('App\Like', $post->likes()->first());
    }

    /**
    * @test
    */
    public function posts_can_count_their_likes()
    {
        $post = factory(Post::class)->create();
        factory(Like::class, 3)->create([
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertEquals(3, $post->likes_count);
    }

    /**
    * @test
    */
    public function posts_know_if_the_authenticated_user_has_liked()
    {
        $this->signIn();
        $firstPost = factory(Post::class)->create();
        factory(Like::class)->create([
            'likeable_id' => $firstPost->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->assertTrue($firstPost->liked);

        $secondPost = factory(Post::class)->create();
        factory(Like::class)->create([
            'likeable_id' => $secondPost->id,
            'likeable_type' => 'App\\Post'
        ]);
        $this->signIn();

        $this->assertFalse($secondPost->liked);
    }

    /**
    * @test
    */
    public function posts_have_dislikes()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $post->likes()->create([
            'user_id' => auth()->id(),
            'dislike' => true
        ]);

        $this->assertInstanceOf('App\Like', $post->dislikes->first());
    }

    /**
    * @test
    */
    public function posts_can_count_their_dislikes()
    {
        $post = factory(Post::class)->create();
        factory(Like::class, 3)->create([
            'likeable_id' => $post->id,
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);
        $this->assertEquals(3, $post->dislikes_count);
    }

    /**
    * @test
    */
    public function posts_know_if_the_authenticated_user_has_disliked()
    {
        $this->signIn();
        $firstPost = factory(Post::class)->create();
        factory(Like::class)->create([
            'likeable_id' => $firstPost->id,
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);
        $this->assertTrue($firstPost->disliked);

        $secondPost = factory(Post::class)->create();
        factory(Like::class)->create([
            'likeable_id' => $secondPost->id,
            'likeable_type' => 'App\\Post',
            'dislike' => true
        ]);
        $this->signIn();
        
        $this->assertFalse($secondPost->disliked);
    }

    /**
    * @test
    */
    public function posts_have_comments()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $post->comments()->create([
            'user_id' => auth()->id(),
            'body' => $this->faker->paragraph
        ]);

        $this->assertInstanceOf('App\Comment', $post->comments()->first());
    }

    /**
    * @test
    */
    public function posts_can_count_their_comments()
    {
        $post = factory(Post::class)->create();
        factory(Comment::class, 3)->create([
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertEquals(3, $post->comments_count);
    }

    /**
    * @test
    */
    public function posts_know_if_the_authenticated_user_has_commented()
    {
        $this->signIn();
        $firstPost = factory(Post::class)->create();
        factory(Comment::class)->create([
            'commentable_id' => $firstPost->id,
            'commentable_type' => 'App\\Post'
        ]);
        $this->assertTrue($firstPost->commented);

        $secondPost = factory(Post::class)->create();
        factory(Comment::class)->create([
            'commentable_id' => $secondPost->id,
            'commentable_type' => 'App\\Post'
        ]);
        $this->signIn();

        $this->assertFalse($secondPost->commented);
    }

    /**
    * @test
    */
    public function users_are_subscribed_to_their_posts_on_creation()
    {
        $this->signIn();
        $post = factory(Post::class)->create();

        $this->assertInstanceOf('App\Subscription', $post->subscriptions()->first());
    }

    /**
    * @test
    */
    public function posts_can_count_their_subscriptions()
    {
        $post = factory(Post::class)->create();
        factory(Subscription::class, 2)->create([
            'subscribable_id' => $post->id,
            'subscribable_type' => 'App\\Post'
        ]);
        $this->assertEquals(3, $post->subscriptions_count);
    }

    /**
    * @test
    */
    public function posts_know_if_the_authenticated_user_has_subscribed()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $this->assertTrue($post->subscribed);

        $this->signIn();
        $this->assertFalse($post->subscribed);
    }

    /**
    * @test
    */
    public function posts_have_favourites()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $post->favourites()->create([
            'user_id' => auth()->id()
        ]);

        $this->assertInstanceOf('App\Favourite', $post->favourites()->first());
    }

    /**
    * @test
    */
    public function posts_can_count_their_favourites()
    {
        $post = factory(Post::class)->create();
        factory(Favourite::class, 3)->create([
            'post_id' => $post->id
        ]);
        $this->assertEquals(3, $post->favourites_count);
    }

    /**
    * @test
    */
    public function posts_know_if_the_authenticated_user_has_favourited()
    {
        $this->signIn();
        $firstPost = factory(Post::class)->create();
        factory(Favourite::class)->create([
            'post_id' => $firstPost->id,
        ]);
        $this->assertTrue($firstPost->favourited);

        $secondPost = factory(Post::class)->create();
        factory(Favourite::class)->create([
            'post_id' => $secondPost->id,
        ]);
        $this->signIn();

        $this->assertFalse($secondPost->favourited);
    }

    /**
    * @test
    */
    public function a_new_post_should_default_to_a_null_published_at_value()
    {
        $post = factory(Post::class)->create();
        $this->assertEquals(null, $post->published_at);
    }

    /**
    * @test
    */
    public function a_post_knows_if_it_has_been_published()
    {
        $this->signIn();
        $post = factory(Post::class)->create();
        $this->assertFalse($post->published);
        $post->update([
            'published_at' => '2018-06-08 15:00:12'
        ]);
        $this->assertTrue($post->published);
    }

    /**
    * @test
    */
    public function a_post_knows_if_the_authenticated_user_is_the_owner()
    {
        $post_1 = factory(Post::class)->create();
        $this->signIn();
        $this->assertFalse($post_1->owner);
        $post_2 = factory(Post::class)->create();
        $this->assertTrue($post_2->owner);
    }
}
