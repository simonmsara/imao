<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\User;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, WithFaker, MakesHttpRequests, RefreshDatabase;

    /**
     * Authenticates a user
     * 
     * @param App\User
     * @return Auth
     */
    public function signIn(User $user = null)
    {
        $user === null ? $this->be(factory('App\User')->create()) : $this->be($user);
        return auth()->user();
    }

    /**
     * Creates a post and returns its instance
     * 
     * @return App\Post
     */
    public function createPost()
    {
        return factory('App\Post')->create();
    }
}
