<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::all()->each(function ($user) {
            factory(App\Post::class, rand(3, 6))->create([
                'user_id' => $user->id,
                'published_at' => Carbon::now()->toDateTimeString()
            ]);
        });
    }
}
