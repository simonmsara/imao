<?php

use App\Post;
use App\User;
use App\Comment;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::all()->each(function ($post) {
            factory(App\Comment::class)->create([
                'user_id' => rand(1,4),
                'commentable_id' => $post->id,
                'commentable_type' => 'App\\Post'
            ]);
        });

        Comment::all()->each(function ($comment) {
            factory(App\Comment::class)->create([
                'user_id' => rand(1,4),
                'commentable_id' => $comment->id,
                'commentable_type' => 'App\\Comment'
            ]);
        });
    }
}
