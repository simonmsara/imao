<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'user_id' => auth()->id() ?: factory(User::class)->create(),
        'title' => $faker->sentence(),
        'abstract' => $faker->paragraphs(5, true),
        'body' => $faker->paragraphs(50, true),
    ];
});
