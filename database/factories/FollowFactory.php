<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Follow::class, function (Faker $faker) {
    return [
        'follower_id' => auth()->id() ?: factory(User::class)->create(),
    ];
});
