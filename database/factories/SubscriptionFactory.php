<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Subscription::class, function (Faker $faker) {
    return [
        'user_id' => auth()->id() ?: factory(User::class)->create()
    ];
});
