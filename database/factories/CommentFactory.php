<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Comment::class, function (Faker $faker) {
    // dd(auth()->id());
    return [
        'user_id' => auth()->id() ?: factory(User::class)->create(),
        'body' => $faker->paragraph()
    ];
});




