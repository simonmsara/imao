<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
        'user_id' => auth()->id() ?: factory(User::class)->create(),
        'description' => $faker->paragraph(),
        'occupation' => $faker->jobTitle(),
        'dob' => $faker->dateTimeThisCentury(),
        'country' => $faker->country()
    ];
});
