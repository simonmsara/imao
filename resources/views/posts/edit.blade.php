@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <form action="{{ url('post/'.$post->id) }}" method="post">
            @method('patch')
            @csrf
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" class="form-control" id="title" aria-describedby="title" placeholder="Enter Title" name="title" value="{{ $post->title }}">
            </div>
            <div class="form-group">
              <label for="abstract">Abstract</label>
              <textarea name="abstract" id="abstract" aria-describedby="abstract" class="form-control" placeholder="Write a short description."rows="4" cols="80">{{ $post->abstract }}</textarea>
            </div>
            <div class="form-group">
              <label for="body">Body</label>
              <textarea name="body" id="body" aria-describedby="body" class="form-control" placeholder="Write something amazing!"rows="8" cols="80">{{ $post->body }}</textarea>
            </div>
            <button type="submit" class="btn btn-success">Save Changes</button>
            </form>

            <hr>

            <form action="{{ url('post/'.$post->id) }}" method="post">
              @method('delete')
              @csrf
              <button type="submit" class="btn btn-outline-danger">Delete Post</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=mpboiqo0dgpr5378v1rxakzculezpy520773f8lem1wrez5s"></script>

  <script>
    tinymce.init({
      selector:'#body',
      menu: {},
      toolbar: 'undo redo | cut copy paste | bold italic underline strikethrough subscript superscript | alignleft aligncenter alignright alignjustify | outdent indent | bullist numlist',
      plugins: "spellchecker",
    });
  </script>
@endsection
