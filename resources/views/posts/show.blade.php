@extends('layouts.app')

@section('content')
<div class="mt-16 p-4">
    <h1 class="text-4xl text-brown leading-none">{{$post->title}}</h1>
    <span class="text-grey">{{$post->created_at}}</span>
    <p class="mt-4">{{$post->body}}</p>
    <user-container :user-info="{{$post->user}}" :post-owner="{{$post->owner ? 'true' : 'false'}}"></user-container>
    <post-interaction :post-data="{{$post}}"></post-interaction>
</div>
@endsection
