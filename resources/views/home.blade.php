@extends('layouts.app')

@section('content')
<div class="w-full p-4 pt-16 flex flex-wrap justify-around">
@foreach($posts as $post)
  <abstract-container :post="{{$post}}"></abstract-container>
@endforeach
</div>
@endsection
