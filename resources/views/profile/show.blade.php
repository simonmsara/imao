@extends('layouts.app')

@section('content')
<div class="mt-10 p-4">
    <user-container :user-info="{{$user}}"></user-container>
    @foreach($user->posts as $post)
        <abstract-container :post="{{$post}}"></abstract-container>
    @endforeach
</div>
@endsection
