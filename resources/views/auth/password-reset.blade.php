<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>{{ config('app.name', 'IMAO') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="This is blogging application that was built to explore the features of Laravel 5.7." name="description" />
        <meta name="auth-check" content="{{auth()->check() ? auth()->id() : false}}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="font-sans">
    <div id="app">
        <flash-container
            v-if="flashActive"
            v-on:close-flash="flashActive = false"
            :flash-content="flashContent"
        ></flash-container>

        <div class="flex justify-center flex-wrap">
            <logo-icon class="m-10 h-16 text-brown fill-current"></logo-icon>
            <password-reset-container></password-reset-container>
        </div>
    </div>    
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
    </body>
</html>