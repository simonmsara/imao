<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>{{ config('app.name', 'IMAO') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="This is blogging application that was built to explore the features of Laravel 5.7." name="description" />
        <meta name="auth-check" content="{{auth()->check() ? auth()->id() : false}}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>

    <body class="font-sans">
    <div id="app">
        @if (auth()->check())
            <nav-container :user="{{auth()->user()->load('profile')}}" :notifications="{{ auth()->check() ? auth()->user()->notifications : '[]'}}"></nav-container>
        @else
            <nav-container></nav-container>
        @endif

        <flash-container
            v-if="flashActive"
            v-on:close-flash="flashActive = false"
            :flash-content="flashContent"
        ></flash-container>

        <div class="container-fluid">
            @yield('content')
        </div>

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        {{-- 2018 © Simon Msara --}}
                    </div>
                </div>
            </div>
        </footer>

    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
    </body>
</html>
