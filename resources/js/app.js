require('./bootstrap')

const requireComponent = require.context('./icons', false, /\.vue$/)
requireComponent.keys().forEach(file => {
    const componentConfig = requireComponent(file)
    const componentName = file.replace('.vue', '').replace('./', '')
    Vue.component(componentName, componentConfig.default)
})

import navContainer from './components/nav-container'
import abstractContainer from './components/abstract-container'
import userContainer from './components/user-container'
import postInteraction from './components/post-interaction'
import flashContainer from './components/flash-container'
import loginRegisterContainer from './components/login-register-container'
import passwordResetContainer from './components/password-reset-container'
import createPostContainer from './components/create-post-container'
import {flashBus} from './flashBus'

new Vue({
    el: '#app',
    components: {
        navContainer,
        abstractContainer,
        userContainer,
        postInteraction,
        flashContainer,
        loginRegisterContainer,
        passwordResetContainer,
        createPostContainer
    },
    data: {
        flashActive: false,
        flashContent: {
            bgColor: 'red',
            textColor: 'white',
            avatarURI: '/avatars/default.svg',
            avatarLink: '/profile/1',
            body: 'There was an error saving your comment.',
            flashLink: '/home'
        }
    },
    mounted(){
        auth = window.auth ? true : false

        flashBus.$on('new-flash', e => {
            this.flashContent = e
        })

        flashBus.$on('error', e => {
            this.showErrorFlash(e)
            setTimeout(()=>{
                this.flashActive = false
            }, 3000)
        })
    },
    methods: {
        showErrorFlash(e){
            this.flashContent.body = e
            this.flashActive = true
        }
    }
});
