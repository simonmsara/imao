import guards from './validation-guards'
/**
 * ---------------------------------------------------------------------------------------------<
 * |    The form validator's main function is to loop through given or all inputs in the [inputs]
 * |    on the [instance] object that is passed into this class. The validator will update
 * |    the [errors] array on the relevant [input]'s [inputProperties] if the validation fails and
 * |    eventually return a boolean of whether there were any validation errors.
 * |
 * |    This validator class imports the [guards] that hold the validation information such as the
 * |    [error], [regex] & negation boolean ([negate]).
 */

export default class formValidator {

    /**
     * Instantiate the form validation and return a validation success boolean
     *
     * @param {Object} instance
     * @param specificInputs
     * @returns {Boolean}
     */
    constructor (instance, specificInputs = []){
        this.instance = instance;
        this.specificInputs = specificInputs;
        this.formValid() ? this.instance.formValid = true : this.instance.formValid = false;
    }

    /**
     * Direct validation to only specific or all inputs and return a validation success boolean
     *
     * @returns {Boolean}
     */
    formValid(){
        return this.specificInputs.length ? this.validateSpecificInputs() : this.validateAllInputs()
    }

    /**
     * Set the [instance]'s [submissionAttempted] property to true
     * Validate only specific [input]s and return a validation success boolean
     *
     * @returns {Boolean}
     */
    validateSpecificInputs(){
        this.instance.submissionAttempted = true
        let formValidationResults = []
        this.specificInputs.forEach(input => {
            let inputProperties = this.instance.inputs[input]
            formValidationResults.push(this.initiateValidation(input, inputProperties))
        })
        return !formValidationResults.includes(false)
    }

    /**
     * Set the [instance]'s [submissionAttempted] property to true
     * Validate all [input]s and return a validation success boolean
     *
     * @returns {Boolean}
     */
    validateAllInputs(){
        this.instance.submissionAttempted = true
        let formValidationResults = []
        for (const [input, inputProperties] of Object.entries(this.instance.inputs)){
            formValidationResults.push(this.initiateValidation(input, inputProperties))
        }
        return !formValidationResults.includes(false)
    }

    /**
     * Check if the relavant [input] has any [rules] and validate accordingly
     * Return a validation success boolean
     *
     * @param {String} input
     * @param {Object} inputProperties
     * @returns {Boolean}
     */
    initiateValidation(input, inputProperties){
        if(inputProperties.validation.length){
            inputProperties.validation.forEach(rule => {
                rule = rule.split(':')
                this.directValidation(input, rule)
            })
            return this.evaluateValidationResults(input)
        }
        return true
    }

    /**
     * Direct the validation based on type
     * @param {Array} rule
     */
    directValidation(input, rule){
        if(rule[0] === 'confirm'){
            return this.validateConfirmation(input, rule[1])
        }
        if(rule[0] === 'unique'){
            return this.validateUnique(input)
        }
        if(rule[0] === 'required'){
            return this.validateRequired(input)
        }
        return this.validate(input ,`${rule[0]}`, rule.length > 1 ? parseInt(rule[1]) : null)
    }

     /**
     * Compare that the [input]'s [value] is identical to the operand [input]'s [value].
     * @param {String} input
     * @param {String} operand
     * @return {Boolean}
     */
    validateConfirmation(input, operand){
        let error = `This doesn't match the ${operand} field.`
        let confirmationValue = this.instance.inputs[input].value
        let operandValue = this.instance.inputs[operand].value
        if(confirmationValue === operandValue){
            this.removeErrorMessage(input, error)
            return true
        }
        this.addValidationError(input, error)
        return false
    }

    /**
     * Check that the [input]'s value does not match the original failed value
     *
     * @param {String} input
     * @return {Boolean}
     */
    validateUnique(input){
        const error = `The ${input} has already been taken.`
        return this.instance.inputs[input].value === this.instance.inputs[input].originalValue ?
        this.addValidationError(input, error) : this.removeErrorMessage(input, error)
    }

    /**
     * Check that the [input]'s value does in not empty
     *
     * @param {String} input
     * @return {Boolean}
     */
    validateRequired(input){
        const error = `This field is required.`
        return this.instance.inputs[input].value == null || this.instance.inputs[input].value.trim() == '' ?
        this.addValidationError(input, error) : this.removeErrorMessage(input, error)
    }

    /**
     * Get the specific [guard] properties from the imported [guards] object
     * Assign the appropriate [error], [regex] & negation value ([negate])
     * Direct validation towards 'count' based validation or exclusivity validation & return evaluation boolean
     *
     * @param {String} input
     * @param {String} guardName
     * @param {Int} minValue
     * @returns {Void}
     */
    validate(input, guardName, minValue){
        let guard = guards()[guardName]
        let error = this.getError(guard, minValue)
        let regex = this.getRegex(guard, minValue)
        let negate = !minValue ? guard.exclusive.negate ? false : true : null
        minValue ? this.validateCount(input,regex, error) : this.validateExclusive(input, regex, error, negate)
    }

    /**
     * Get the relevant [errorMessage] for the specific [input] based on the validation type
     *
     * @param {Object} guard
     * @param {Int} minValue
     * @returns {String}
     */
    getError(guard, minValue){
        if(minValue){
            return minValue === 1 ? guard.counted.error.singular :
            guard.counted.error.plural.replace(':value', minValue)
        }
        return guard.exclusive.error
    }

    /**
     * Get the relevant [regex] for the specific [input] based on the validation type
     *
     * @param {Object} guard
     * @param {Int} minValue
     * @returns {Boolean}
     */
    getRegex(guard, minValue){
        if(minValue){
            return guard.counted.regex.replace(':value', minValue)
        }
        return guard.exclusive.regex
    }

    /**
     * Check if the [input]'s [value] matches the [regex] and use the [negate] value to return the correct interpretation of the match
     * Add or remove validation errors based on assessment and return validation success boolean
     *
     * @param {String} input
     * @param {String|Regex} regex
     * @param {String} error
     * @param {Boolean|Void} negate
     */
    validateExclusive(input, regex, error, negate){
        if((this.instance.inputs[input].value.match(regex) !== null) === negate){
            this.addValidationError(input, error)
            return false
        }
        this.removeErrorMessage(input, error)
        return true
    }

    /**
     * Check if the [input]'s [value] matches the [regex] value to return the correct interpretation of the match
     * Add or remove validation errors based on assessment and return validation success boolean
     *
     * @param {String} input
     * @param {String|Regex} regex
     * @param {String} error
     * @return {Boolean}
     */
    validateCount(input, regex, error){
        if(this.instance.inputs[input].value.match(regex)){
            this.removeErrorMessage(input, error)
            return true
        }
        this.addValidationError(input, error)
        return false
    }

    /**
     * Check if the specific [input] has that [errorMessage] and push it if if doesn't
     *
     * @param {String} input
     * @param {String} errorMessage
     * @returns {Void}
     */
    addValidationError(input, errorMessage){
        this.instance.inputs[input].errors.includes(errorMessage) ? null :
        this.instance.inputs[input].errors.push(errorMessage)
    }

    /**
     * Removes validation [errorMessage] from [input]'s [errors] array
     *
     * @param {String} input
     * @param {String} errorMessage
     * @return {Void}
     */
    removeErrorMessage(input, errorMessage){
        let errorIndex = this.instance.inputs[input].errors.indexOf(errorMessage)
        errorIndex !== -1 ? this.instance.inputs[input].errors.splice(errorIndex, 1) : null
    }

    /**
     * Check if the specific [input] has any errors and update its [hasErrors] property
     *
     * @param {String} input
     * @returns {Boolean}
     */
    evaluateValidationResults(input){
        if(this.instance.inputs[input].errors.length > 0){
            this.instance.inputs[input].hasErrors = true
            return false
        }
        this.instance.inputs[input].hasErrors = false
        return true
    }
}
