import {capitalizeFirstLetter} from '../helpers'
import { emitError } from '../helpers'
/**
 * Like all likeable elements of the applications
 */
export default class likeOrDislike {
    /**
     * Set the instance of the caller
     * Set the subject to post or comment
     * Set target to like or dislike
     * 
     * @param {Object} instance
     * @param {String} subject
     * @param {String} target
     * @returns {Void}
     */
    constructor (instance, subject, target){
        this.instance = instance
        this.subject = subject
        this.target = target
        this.setOriginalState()
        this.likeOrDislike()
    }

    /**
     * Set the original state incase of failure to persist
     * @@return {Void}
     */
    setOriginalState() {
        this.originalState = {
            liked: this.instance[this.subject].liked,
            likes_count: this.instance[this.subject].likes_count,
            disliked: this.instance[this.subject].disliked,
            dislikes_count: this.instance[this.subject].dislikes_count
        }
    }

    /**
     * Like or unlike the subject based on its current state
     * @returns {Void}
     */
    likeOrDislike(){
        let url = this.target === 'like' ?
        this.instance[this.subject].liked ? '/unlike' : '/like' :
        this.instance[this.subject].disliked ? '/unlike' : '/dislike'

        axios.post(url, {
            likeable_id: this.instance[this.subject].id,
            likeable_type: `App\\${capitalizeFirstLetter(this.subject)}`
        }).catch(E => {
            emitError(E)
            this.revertState()
        })
        this.target === 'like' ? this.updateLikeState() : this.updateDislikeState()
    }

    /**
     * Update the instance to match the database
     * @returns {Void}
     */
    updateLikeState(){
        if(this.instance[this.subject].liked) {
            this.instance[this.subject].liked = false
            this.instance[this.subject].likes_count--
        }else{
            this.instance[this.subject].liked = true
            this.instance[this.subject].likes_count++
            this.instance[this.subject].disliked ? this.instance[this.subject].dislikes_count-- : null
            this.instance[this.subject].disliked = false
        }
    }

    /**
     * Update the instance to match the database
     * @returns {Void}
     */
    updateDislikeState(){
        if(this.instance[this.subject].disliked) {
            this.instance[this.subject].disliked = false
            this.instance[this.subject].dislikes_count--
        }else{
            this.instance[this.subject].disliked = true
            this.instance[this.subject].dislikes_count++
            this.instance[this.subject].liked ? this.instance[this.subject].likes_count-- : null
            this.instance[this.subject].liked = false
        }
    }

    /**
     * Revert the instance back to its original state
     * @returns {Void}
     */
    revertState(){
        this.instance[this.subject].liked = this.originalState.liked
        this.instance[this.subject].likes_count = this.originalState.likes_count
        this.instance[this.subject].disliked = this.originalState.disliked
        this.instance[this.subject].dislikes_count = this.originalState.dislikes_count
    }
}