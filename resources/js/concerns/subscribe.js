import {emitError} from '../helpers'
import {capitalizeFirstLetter} from '../helpers'

/**
 * Persist the subscription state to a subscribable item
 */
export default class subscribe {

    /**
     * Assign the instance of the subscribable object
     * The subject is the item being subscribed to
     * @param {Object} instance 
     * @param {String} subject 
     */
    constructor(instance, subject){
        this.instance = instance
        this.subject = this.instance[subject]
        this.subjectName = subject
        this.subscribe()
    }

    /**
     * Persist the new subscription state
     * @returns {Void}
     */
    subscribe(){
        let uri = this.subject.subscribed ? '/unsubscribe' : '/subscribe'
        axios.post(uri, {
            subscribable_id: this.subject.id,
            subscribable_type: `App\\${capitalizeFirstLetter(this.subjectName)}`
        }).catch(E => {
            emitError(E)
            this.subject.subscribed = !this.subject.subscribed
        })
        this.subject.subscribed = !this.subject.subscribed
    }
}