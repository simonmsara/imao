import { capitalizeFirstLetter } from '../helpers'
import { emitError } from '../helpers'

/**
 * Comment on all commentable elements of the application
 */
export default class comment {
    /**
     * Assign the instance of the commentable object
     * The subject is the item being commented on
     * @param {Object} instance 
     * @param {String} subject 
     */
    constructor(instance, subject) {
        this.instance = instance
        this.subject = this.instance[subject]
        this.subjectName = subject
        this.setOriginalState()
        this.comment()
    }

    /**
     * Set the original state incase of failure to persist
     * @@return {Void}
     */
    setOriginalState() {
        this.originalComment = this.comment
    }

    /**
     * Comment on the subject
     * @returns {Void}
     */
    comment() {
        if (this.newComment !== '') {
            axios.post('/comment', {
                body: this.instance.inputs.comment.value,
                commentable_id: this.subject.id,
                commentable_type: `App\\${capitalizeFirstLetter(this.subjectName)}`
            }).then(R => {
                this.updateCommentsArray(R.data)
            }).catch(E => {
                this.addServerSideErrors(E)
            })
        }
    }

    /**
         * Add serverside errors 
         * 
         * @returns {Void}
         */
        addServerSideErrors(E){
            emitError(E)
            for (const [input, inputProperties] of Object.entries(this.instance.inputs)){
                if(E.response.data.errors.hasOwnProperty(input)){
                    this.instance.inputs[input].errors = E.response.data.errors[input]
                    this.instance.inputs[input].hasErrors = true
                    this.instance.inputs[input].originalValue = JSON.parse(JSON.stringify(this.instance.inputs[input].value))
                    this.instance.formValid = false
                }
            }
            if(E.response.data.errors.hasOwnProperty('form')){
                this.instance.form.errors = E.response.data.errors.form
                this.instance.form.hasErrors = true
            }
        }

    /**
     * Direct to appropriate method based on subject
     * @param {Object} savedComment
     * @returns {Void}
     */
    updateCommentsArray(savedComment) {
        if (this.subjectName === 'post') {
            this.addToPostComments(savedComment)
        } else {
            this.addToCommentReplies(savedComment)
        }
    }

    /**
     * Update the comment's replies array to match the DB
     * @param {Object} savedComment 
     * @returns {Void}
     */
    addToPostComments(savedComment) {
        this.instance.inputs.comment.value = ''
        this.instance.comments.unshift(savedComment)
        this.subject.comments_count++
    }

    /**
     * Update the post's comments array to match the DB
     * @param {Object} savedComment 
     * @returns {Void}
     */
    addToCommentReplies(savedComment) {
        this.instance.inputs.comment.value = ''
        this.instance.comment.replies_count++
        this.instance.comment.replies.push(savedComment)
        this.instance.$nextTick(() => {
            this.scrollToBottom()
        })
    }


    /**
     * Scroll to the bottom of the replies container when a new reply is added
     * @returns {Void}
     */
    scrollToBottom(){
        let container = document.getElementById(`replies-container-${this.instance.comment.id}`)
        container.scrollTop = container.scrollHeight
    }
}