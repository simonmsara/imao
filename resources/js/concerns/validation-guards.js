/**
 * THis function returns an object of all available guards which contain
 * the errors, regular expressions and negation instructions
 * 
 * @returns {Object}
 */
export default function guard(){
    return {
        /**
         * Letters of the alphabet bith upper and lower case
         */
        alpha: {
            exclusive: {
                error: 'This may only contain letters.',
                regex: /[^a-zA-Z ]/,
                negate: false
            },
            counted: {
                error: {
                    singular: 'This must contain at least 1 letter.',
                    plural: 'This must contain at least :value letters.'
                },
                regex: '(.*[a-zA-Z].*){:value,}'
            }
        },
        /**
         * Valid email address
         */
        email: {
            exclusive: {
                error: 'This must be a valid email address.',
                regex: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                negate: true
            }
        },
        /**
         * Numbers 0-9
         */
        number: {
            exclusive: {
                error: 'This may only contain numbers.',
                regex: /[^0-9 ]/,
                negate: false
            },
            counted: {
                error: {
                    singular: 'This must contain at least 1 number.',
                    plural: 'This must contain at least :value numbers.'
                },
                regex: '(.*[0-9].*){:value,}'
            }
        },
        /**
         * Lowercase letters of the alphabet
         */
        lower: {
            exclusive: {
                error: 'This may only contain lower case letters.',
                regex: /[^a-z ]/,
                negate: false
            },
            counted: {
                error: {
                    singular: 'This must contain at least 1 lower case letter.',
                    plural: 'This must contain at least :value lower case letters.'
                },
                regex: '(.*[a-z].*){:value,}'
            }
        },
        /**
         * Minimum numbers of characters
         */
        min: {
            counted: {
                error: {
                    singular: 'This must be a minimum of 1 character long.',
                    plural: 'This must be a minimum of :value characters long.'
                },
                regex: '(.){:value,}'
            }
        },
        /**
         * Strings that are at least 1 character long
         */
        required: {
            exclusive: {
                error: 'This field is required.',
                regex: /^$|\s+/,
                negate: false
            }
        },
        /**
         * Special charactors that are not a number, letter or space
         */
        special: {
            exclusive: {
                error: 'This may only contain special characters.',
                regex: /[a-zA-Z0-9 ]/,
                negate: false
            },
            counted: {
                error: {
                    singular: 'This must contain at least 1 special character.',
                    plural: 'This must contain at least :value special characters.'
                },
                regex: '(.*[^a-zA-Z0-9 ].*){:value,}'
            }
        },
        /**
         * Uppercase letters ot the alphabet
         */
        upper: {
            exclusive: {
                error: 'This may only contain capital letters.',
                regex: /[^A-Z ]/,
                negate: false
            },
            counted: {
                error: {
                    singular: 'This must contain at least 1 capital letter.',
                    plural: 'THis must contain at least :value capital letters.'
                },
                regex: '(.*[A-Z].*){:value,}'
            }
        }
    }
}