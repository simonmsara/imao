import { flashBus } from './flashBus'
import moment from 'moment'

    /**
     * Capitalize the first letter of the word
     * @param {String} word
     * @returns {String} word
     */
    export function capitalizeFirstLetter(word){
        let subject = word.trim()
        return subject.charAt(0).toUpperCase() + subject.slice(1).toLowerCase()
    }

    /**
     * Emit an error event on the flashBus and pass the relevant message
     * @param {Object} E
     * @returns {Void}
     */
    export function emitError(E) {
        let message = E.response.data.message
        flashBus.$emit('error', message ? message : 'Sorry! Something went wrong.')
    }

    /**
     * Convert to human readable time 
     * @param {String} time 
     * @returns {String}
     */
    export function convertTime(time){
        return moment(time).fromNow()
    }

    /**
     * Parse the values of a query string and return a query object
     * 
     * @param {String} search
     * @return {Object}
     */
    export function getURLQueries(){
        let locationLessOrigin = location.href.replace(`${location.origin}`, '')
        let locationLessPathname = locationLessOrigin.replace(`${location.pathname}`, '')
        let keyValuePairs = locationLessPathname ? locationLessPathname.replace(`?`, '').split('&') : ''

        let queryObject = {
            pathname: location.pathname
        }
        
        if(keyValuePairs !== ''){
            keyValuePairs.forEach(keyValue => {
                let splitString = keyValue.split('=')
                queryObject[splitString[0]] = splitString.length > 1 ? splitString[1] : null
            });
        }
        return queryObject
    }