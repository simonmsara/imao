export default {
    brown: {
      lighter: '',
      default: '#A66B58',
      dark: ''
    },
    green: {
      lightest: '#a3ffee',
      lighter: '#5effe2',
      light: '#3ef0d0',
      default: '#14DBB7',
      dark: '#11a88d',
      darker: '#0c7865',
      darkest: '#085447'
    },
    red: {
      lighter: '',
      default: '#EB3452',
      dark: ''
    },
    amber: {
      lighter: '',
      default: '#F7A820',
      dark: ''
    },
    blue: {
      lighter: '',
      default: '#2CA4DB',
      dark: ''
    },
    black: {
      default: '#33211B'
    },
    grey: {
      lighter: '',
      default: '#DDD',
      dark: ''
    }
  }