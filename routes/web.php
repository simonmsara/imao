<?php
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('home');
});

Auth::routes();

Broadcast::routes();

Route::middleware('auth')->group(function() {
  Route::patch('user', 'UserController@update')->name('user.update');
  Route::delete('user', 'UserController@destroy')->name('user.destroy');

  Route::post('avatar', 'AvatarController@store')->name('avatar.store');
  Route::delete('avatar', 'AvatarController@destroy')->name('avatar.destroy');

  Route::get('profile/{user}', 'ProfileController@show')->name('profile.show');
  Route::post('profile', 'ProfileController@update')->name('profile');

  Route::get('follow/{user}', 'FollowController@store');
  Route::delete('follow/{user}', 'FollowController@destroy');

  Route::post('comment', 'CommentController@store')->name('comment.store'); //Save Post Comments
  Route::patch('comment/{comment}', 'CommentController@update')->name('comment.update'); //save edited comment
  Route::delete('comment/{comment}', 'CommentController@destroy')->name('comment.destroy'); // delete comment

  Route::post('reply', 'ReplyController@store')->name('reply.store'); //Save Comment replys
  Route::patch('reply/{reply}', 'ReplyController@update')->name('reply.update'); //save edited reply
  Route::delete('reply/{reply}', 'ReplyController@destroy')->name('reply.destroy'); // delete reply

  Route::post('/like', 'LikeController@like');
  Route::post('/dislike', 'LikeController@dislike');
  Route::post('/unlike', 'LikeController@unlike');
  

  Route::post('/favourite', 'FavouriteController@store');
  Route::post('/unfavourite', 'FavouriteController@destroy');

  Route::post('/subscribe', 'SubscriptionController@store');
  Route::post('/unsubscribe', 'SubscriptionController@destroy');

  Route::post('/publish/{post}', 'PostController@publish')->name('publish');
  Route::get('unpublish/{post}', 'PostController@unpublish')->name('unpublish');

  Route::get('/notifications/read', function(){
    auth()->user()->unreadNotifications->markAsRead();
  });
});

Route::get('home', 'PostController@index')->name('home');

Route::resource('post', 'PostController');

Route::post('search', 'SearchController@search')->name('search');









