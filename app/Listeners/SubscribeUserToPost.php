<?php

namespace App\Listeners;

use App\Events\PostCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Subscription;

class SubscribeUserToPost
{
    /**
     * Handle the event.
     *
     * @param  PostCreated  $event
     * @return void
     */
    public function handle(PostCreated $event)
    {
        // auth()->user()->subscriptions()->create([
        //     'subscribable_id' => $event->post->id,
        //     'subscribable_type' => 'App\\Post'
        // ]);
        Subscription::create([
            'user_id' => auth()->id() ?: $event->post->user_id,
            'subscribable_id' => $event->post->id,
            'subscribable_type' => 'App\\Post'
        ]);
    }
}
