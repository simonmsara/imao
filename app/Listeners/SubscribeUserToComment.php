<?php

namespace App\Listeners;

use App\Subscription;
use App\Events\CommentCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribeUserToComment
{
    /**
     * Handle the event.
     *
     * @param  CommentSaved  $event
     * @return void
     */
    public function handle(CommentCreated $event)
    {
        Subscription::create([
            'user_id' => auth()->id() ?: $event->comment->user_id,
            'subscribable_id' => $event->comment->id,
            'subscribable_type' => 'App\\Comment'
        ]);

        
    }
}
