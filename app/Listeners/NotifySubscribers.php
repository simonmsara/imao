<?php

namespace App\Listeners;

use App\User;
use App\Events\CommentCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Collection;
use App\Comment;
use App\Notifications\CommentWasCreated;
use App\Notifications\CommentWasRepliedTo;
use App\Subscription;

class NotifySubscribers
{
    /**
     * Handle the event.
     *
     * @param  CommentCreated $event
     * @return void
     */
    public function handle(CommentCreated $event)
    {
        $comment = $event->comment;
        $subscriptions = Subscription::where([
            ['subscribable_type', '=', $comment->commentable_type],
            ['subscribable_id', '=', $comment->commentable_id]
        ])->get();
        $subscriptions->each(function ($subscriber) use ($comment) {
            User::find($subscriber->user_id)->notify(new CommentWasCreated($comment));
        });
    } 
}
