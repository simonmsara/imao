<?php

namespace App\Providers;

use App\Post;
use App\Like;
use App\Comment;
use App\Policies\LikePolicy;
use App\Policies\PostPolicy;
use App\Policies\CommentPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Favourite;
use App\Policies\FavouritePolicy;
use App\Follow;
use App\Policies\FollowPolicy;
use App\Subscription;
use App\Policies\SubscriptionPolicy;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
         Post::class => PostPolicy::class,
         Comment::class => CommentPolicy::class,
         Like::class => LikePolicy::class,
         Favourite::class => FavouritePolicy::class,
         Follow::class => FollowPolicy::class,
         Subscription::class => SubscriptionPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::resource('posts', 'App\Policies\PostPolicy');
        Gate::resource('comment', 'App\Policies\CommentPolicy');
        Gate::define('reply.owner', 'App\Policies\ReplyPolicy@owner');
    }
}
