<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\PostCreated;
use Carbon\Carbon;

class Post extends Model
{
  protected $guarded = ['id', 'created_at', 'updated_at'];

  protected $dispatchesEvents = [
    'created' => PostCreated::class
];

  protected $appends = [
    'likes_count', 'liked',
    'dislikes_count', 'disliked',
    'favourites_count', 'favourited',
    'comments_count', 'commented',
    'subscriptions_count', 'subscribed', 
    'path', 'owner'];

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function getPathAttribute()
  {
    return '/post/' . $this->id;
  }

  public function getPublishedAttribute()
  {
    return $this->published_at !== null && $this->published_at <= date('Y-m-d H:i:s');
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::create($value)->diffForHumans();
  }

  public function subscriptions()
  {
    return $this->morphMany(Subscription::class, 'subscribable');
  }
  public function getSubscriptionsCountAttribute()
  {
    return $this->subscriptions()->count();
  }
  public function getSubscribedAttribute()
  {
      return $this->subscriptions()->where('user_id', '=', auth()->id())->exists();
  }


  public function comments(){
    return $this->morphMany(Comment::class, 'commentable');
  }
  public function getCommentsCountAttribute(){
    return $this->comments()->count();
  }
  public function getCommentedAttribute(){
      return $this->comments()->where('user_id', '=', auth()->id())->exists();
  }

  public function likes() {
    return $this->morphMany(Like::class, 'likeable')->where('dislike', false);
  }
  public function getLikesCountAttribute() {
      return $this->likes()->count();
  }
  public function getLikedAttribute() {
      return $this->likes()->where('user_id', auth()->id())->exists();
  }

  public function dislikes(){
    return $this->morphMany(Like::class, 'likeable')->where('dislike', true);
  }
  public function getDislikesCountAttribute() {
      return $this->dislikes()->count();
  }
  public function getDislikedAttribute(){
      return $this->dislikes()->where('user_id', auth()->id())->exists();
  }

  public function favourites(){
      return $this->hasMany(Favourite::class);
  }
  public function getFavouritesCountAttribute(){
      return $this->favourites()->count();
  }
  public function getFavouritedAttribute(){
    return $this->favourites()->where('user_id', '=', auth()->id())->exists();
  }

  public function getOwnerAttribute()
  {
    return (int) $this->user_id === (int) auth()->id();
  }
}
