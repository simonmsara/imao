<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\CommentCreated;
use Illuminate\Support\Carbon;

class Comment extends Model
{
    protected $dispatchesEvents = [
        'created' => CommentCreated::class
    ];

    protected $guarded = [];

    protected $appends = [
        'owner',
        'replies_count','replied',
        'likes_count', 'liked',
        'dislikes_count', 'disliked', 
        'subscriptions_count', 'subscribed'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function getRepliesCountAttribute()
    {
        return $this->replies()->count();
    }
    public function getRepliedAttribute()
    {
        return $this->replies()->where('user_id', auth()->id())->exists();
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable')->where('dislike', false);
    }
    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }
    public function getLikedAttribute()
    {
        return $this->likes()->where('user_id', '=', auth()->id())->exists();
    }

    public function dislikes()
    {
        return $this->morphMany(Like::class, 'likeable')->where('dislike', true);
    }
    public function getDislikesCountAttribute()
    {
        return $this->dislikes()->count();
    }
    public function getDislikedAttribute()
    {
        return $this->dislikes()->where('user_id', auth()->id())->exists();
    }

    public function subscriptions()
    {
      return $this->morphMany(Subscription::class, 'subscribable');
    }
    public function getSubscribedAttribute()
    {
        return $this->subscriptions()->where('user_id', auth()->id())->exists();
    }
    public function getSubscriptionsCountAttribute()
    {
        return $this->subscriptions()->count();
    }

    public function getOwnerAttribute()
    {
        return $this->user_id === auth()->id();
    }

    public function getCreatedAtAttribute($value)
  {
    return Carbon::create($value)->diffForHumans();
  }
}
