<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $guarded = [];

    protected $casts = [
        'follower_id' => 'integer',
        'leader_id' => 'integer'
    ];
}
