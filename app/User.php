<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Events\UserCreated;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'avatar'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'fullname', 'path',
        'followers_count', 'followed',
        'leaders_count', 'lead',
        'posts_count', 'comments_count',
        'published_posts_count', 'unpublished_posts_count'
    ];

    public function getPathAttribute()
    {
        return url('profile') . '/' . $this->id;
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'follows', 'leader_id', 'follower_id')->withTimestamps();
    }
    public function getFollowersCountAttribute()
    {
        return $this->followers()->count();
    }
    public function getFollowedAttribute()
    {
        return $this->followers()->where('follower_id', '=', auth()->id())->exists();
    }

    public function leaders()
    {
        return $this->belongsToMany(User::class, 'follows', 'follower_id', 'leader_id')->withTimestamps();
    }
    public function getLeadersCountAttribute()
    {
        return $this->leaders()->count();
    }
    public function getLeadAttribute()
    {
        return $this->leaders()->where('leader_id', auth()->id())->exists();
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    public function getPostsCountAttribute()
    {
        return $this->posts()->count();
    }

    public function publishedPosts()
    {
        return $this->posts()->where('published_at', '>=', now());
    }
    public function getPublishedPostsCountAttribute()
    {
        return $this->publishedPosts()->count();
    }

    public function unpublishedPosts()
    {
        return $this->posts()->where([
            ['published_at', '<', now(), 'OR'],
            ['published_at', '=', null, 'OR']
        ]);
    }
    public function getUnpublishedPostsCountAttribute()
    {
        return $this->unPublishedPosts()->count();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }

    public function replies()
    {
        return $this->hasMany(Comment::class)->where('commentable_type', 'App\\Comment');
    }
    public function getRepliesCountAttribute()
    {
        return $this->replies()->count();
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
    public function getSubscriptionsCountAttribute()
    {
        return $this->subscriptions()->count();
    }

    public function postSubscriptions()
    {
        return $this->hasMany(Subscription::class)->where('subscribable_type', 'App\\Post');
    }
    public function getPostSubscriptionsCountAttribute()
    {
        return $this->postSubscriptions()->count();
    }

    public function commentSubscriptions()
    {
        return $this->hasMany(Subscription::class)->where('subscribable_type', 'App\\Comment');
    }
    public function getcommentSubscriptionsCountAttribute()
    {
        return $this->commentSubscriptions()->count();
    }

    public function likes()
    {
        return $this->hasMany(Like::class)->where('dislike', false);
    }
    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }

    public function dislikes()
    {
        return $this->hasMany(Like::class)->where('dislike', true);
    }
    public function getDislikesCountAttribute()
    {
        return $this->dislikes()->count();
    }

    public function getFullnameAttribute()
    {
        return "{$this->firstname} {$this->lastname}";
    }

    public function getAvatarAttribute($value)
    {
        return asset($value);
    }
}
