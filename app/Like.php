<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $guarded = [];
    
    protected $casts = [
        'dislike' => 'boolean',
        'user_id' => 'integer'
    ];

    public function likeable() {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
