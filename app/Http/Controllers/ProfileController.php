<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = $user->load([
            'profile',
            'leaders',
            'posts' => function($query) {
                $query->orderBy('created_at', 'desc')->with('user');
        }]);
        return view('profile.show')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        auth()->user()->profile()->update($this->validator());
    }

    protected function validator()
    {
        return request()->validate([
            'description' => 'string',
            'occupation' => 'string',
            'country' => 'string',
            'dob' => 'date'
        ]);
    }
}
