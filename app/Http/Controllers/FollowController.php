<?php

namespace App\Http\Controllers;

use App\User;
use App\Follow;

class FollowController extends Controller
{
    public function store(User $user) {
        $this->validateRequest($user);
        Follow::create([
            'leader_id' => $user->id,
            'follower_id' => auth()->id()
        ]);
        return $user->fresh();
    }

    public function destroy(Int $leaderId){
        if(!$this->followed($leaderId)){
            abort(404);
        }

        $follow = Follow::where([
            'follower_id' => auth()->id(),
            'leader_id' => $leaderId
        ])->firstOrFail();

        $this->authorize('delete', $follow);

        $follow->delete();
        return User::find($leaderId);
    }

    protected function validateRequest($user){
        $this->followed($user->id) ? abort(400, 'You already follow them') :
            auth()->id() != $user->id ?: abort(400, 'You obviously can\'t follow yourself!');
        
    }

    public function followed($leaderId) {
        return auth()->user()->leaders()->where('leader_id', $leaderId)->first();
    }
}
