<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Http\Response;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validated = $this->validateComment();
        $this->validateReferer($validated);
        $savedComment = auth()->user()->comments()->create($validated)
                        ->load(['user','replies']);

        return response()->json($savedComment);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Comment $comment
     * @return Comment
     * @throws AuthorizationException
     */
    public function update(Request $request, Comment $comment)
    {
        $this->authorize('update', $comment);
        $comment->update($request->validate(['body' => 'required']));
        return $comment->load(['user', 'replies.user']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);
        $comment->delete();
    }

    protected function validateComment()
    {
        return request()->validate([
            'body' => 'required',
            'commentable_id' => 'required',
            'commentable_type' => 'required'
        ]);
    }

    protected function validateReferer($validated)
    {
        $referer = explode('/', request()->header('referer'));
        if($validated['commentable_type'] === 'App\\Post'){
            $referer[3] === 'post' && (int) $referer[4] === (int) $validated['commentable_id'] ?:
            abort(403, 'Can\'t do that mate\!');
            Post::findOrFail($validated['commentable_id']);
        }

        if($validated['commentable_type'] === 'App\\Comment'){
            $parentComment = Comment::findOrFail($validated['commentable_id']);
            $referer[3] === 'post' && (int) $referer[4] === (int) $parentComment->commentable_id ?:
            abort(403, 'Can\'t do that mate\!');
        }
    }
}
