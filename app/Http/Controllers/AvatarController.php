<?php

namespace App\Http\Controllers;

class AvatarController extends Controller
{
    public function store()
    {
        request()->validate([
            'avatar' => 'image'
        ]);

        auth()->user()->update([
            'avatar' => request()->file('avatar')->store('avatars', 'public')
        ]);
    }
}
