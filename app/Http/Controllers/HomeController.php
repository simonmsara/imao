<?php

namespace App\Http\Controllers;

use App\Controllers\PostController;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //...
    }
}
