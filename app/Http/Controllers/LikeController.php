<?php

namespace App\Http\Controllers;

use App\Like;
use App\Post;
use App\Comment;

class LikeController extends Controller
{
    public function like()
    {
        $this->validateRequest();

        Like::updateOrCreate([
            'user_id' => auth()->id(),
            'likeable_id' => request()->likeable_id,
            'likeable_type' => request()->likeable_type
        ],
        [
            'dislike' => false
        ]);
    }

    public function dislike()
    {
        $this->validateRequest();

        Like::updateOrCreate([
            'user_id' => auth()->id(),
            'likeable_id' => request()->likeable_id,
            'likeable_type' => request()->likeable_type
        ],
        [
            'dislike' => true
        ]);
    }

    protected function unlike() {
        $like = Like::where([
            'user_id' => auth()->id(),
            'likeable_id' => request()->likeable_id,
            'likeable_type' => request()->likeable_type
        ])->firstOrFail();
        
        $this->authorize('delete', $like);
        $like->delete();
    }

    protected function validateRequest(){
        request()->validate([
            'likeable_id' => 'required|integer',
            'likeable_type' => 'required|string'
        ]);

        $this->likeableObjectExists();
    }

    protected function likeableObjectExists()
    {
        if(request()->likeable_type == 'App\\Post'){
            Post::findOrFail(request()->likeable_id);
        }

        if(request()->likeable_type == 'App\\Comment'){
            Comment::findOrFail(request()->likeable_id);
        }
    }
}
