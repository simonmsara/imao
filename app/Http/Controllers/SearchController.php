<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $users = $this->findUsers($request->phrase);
        $posts = $this->findPosts($request->phrase);
        return [
            'authors' => $users ?: [],
            'posts' => $posts ?: []
        ];
    }

    protected function findUsers(String $phrase)
    {
        $phrase = '%' . $phrase . '%';
        $query = (new User())->newQuery();
        $query = $query->where('firstname', 'LIKE', $phrase, 'OR');
        $query = $query->where('lastname', 'LIKE', $phrase, 'OR');
        $query = $query->whereHas('profile', function ($query) use ($phrase) {
            return $query->where('country', 'LIKE', $phrase);
        }, 'OR');
        $query = $query->whereHas('profile', function ($query) use ($phrase) {
            return $query->where('occupation', 'LIKE', $phrase);
        }, 'OR');
        return $query->get()->load('profile');
    }

    protected function findPosts(String $phrase)
    {
        $phrase = '%' . $phrase . '%';
        $query = (new Post())->newQuery();
        $query = $query->where('title', 'LIKE', $phrase, 'OR');
        $query = $query->where('abstract', 'LIKE', $phrase, 'OR');
        return $query->get()->load('user');
    }
}
