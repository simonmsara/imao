<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\Comment;
use Illuminate\Http\Request;
use App\Post;

class SubscriptionController extends Controller
{
    public function store(Request $request){
        $validatedRequest = $this->validateRequest($request);
        auth()->user()->subscriptions()->create($validatedRequest);
        return response('subscribed', 200);
    }

    public function destroy(){
        $subscription = Subscription::where([
            'user_id' => auth()->id(),
            'subscribable_id' => request('subscribable_id'),
            'subscribable_type' => request('subscribable_type')
        ])->firstOrFail();
        $this->authorize('owner', $subscription);
        $subscription->delete();
        return response('unsubscribed');
    }

    protected function validateRequest(){
        $validatedRequest = request()->validate([
            'subscribable_id' => 'required|integer',
            'subscribable_type' => 'required|string'
        ]);
        $this->subscribable($validatedRequest) ?: abort(303, 'You cannot subscribe to a reply');
        $this->unique($validatedRequest) ?: abort(303, 'You\'re already subscribed');
        $this->validateReferer($validatedRequest) ?: abort(403, 'Can\'t do that mate!');
        return $validatedRequest;
    }

    protected function subscribable($uniqueRequest){
        if($uniqueRequest['subscribable_type'] === 'App\\Post') {
            return Post::findOrFail($uniqueRequest['subscribable_id']);
        }
        $comment = Comment::findOrFail($uniqueRequest['subscribable_id']);
        // dd(auth()->id());88
        return $comment->commentable_type !== 'App\\Comment' || $comment->owner;
    }

    protected function unique($validatedRequest){
        return ! Subscription::where([
            'user_id' => auth()->id(),
            'subscribable_id' => $validatedRequest['subscribable_id'],
            'subscribable_type' => $validatedRequest['subscribable_type']
        ])->exists();
    }

    protected function validateReferer($validated){
        $referer = explode('/', request()->header('referer'));
        if($validated['subscribable_type'] === 'App\\Post'){
            return $referer[3] === 'post' && (int) $referer[4] === (int) $validated['subscribable_id'];
        }
        $comment = Comment::find($validated['subscribable_id']);
        if($comment->commentable_type === 'App\\Post'){
            return $referer[3] === 'post' && (int) $referer[4] === (int) $comment->commentable_id;
        }
        return $referer[3] === 'post' && (int) $referer[4] === (int) $comment->commentable->commentable_id;
    }
}
