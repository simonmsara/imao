<?php

/*
* Controller is used to manage HTTP requests about poats.
*/

namespace App\Http\Controllers;

use App\User;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['show']);
    }

    public function index(){
        $posts = Post::where([['published_at', '<=', date('Y-m-d H:i:s')]])
            ->orderBy('created_at', 'desc')
            ->get()->load('user');

        return view('home')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validated = $this->validatePost();

        return redirect(
            auth()->user()->posts()->create($validated)->path()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        if($post->published){
            $post = $post->load([
                'user',
                'comments' => function($query) {
                    $query->orderBy('created_at', 'desc')->with('user');
                },
                'comments.replies.user'
            ]);
            return view('posts.show')->with('post', $post);
        }else{
            if((int) $post->user_id === auth()->id()){
                return redirect('post/'.$post->id.'/edit', 302);
            }else{
                abort(404);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $this->authorize('owner', $post);

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Post $post)
    {
        $validated = $this->validatePost();
        $this->authorize('owner', $post);
        $post->update($validated);
        return redirect('post/'.$post->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function publish(Post $post)
    {
        $this->authorize('owner', $post);
        request()->validate([ 'datetime' => 'date_format:"Y-m-d H:i:s"' ]);
        $customDatetime = request()->datetime > Carbon::now()->toDateTimeString() ? request()->datetime : null;

        $post->update([
            'published_at' => $customDatetime ?: Carbon::now()->toDateTimeString()
        ]);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function unpublish(Post $post)
    {
        $this->authorize('owner', $post);
        $post->update([
            'published_at' => null
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
      $this->authorize('owner', $post);
      $post->delete();
      return redirect('home');
    }

    protected function validatePost()
    {
        return request()->validate([
        'title' => 'required|max:255',
        'abstract' => 'max:400',
        'body' => 'required'
        ]);
    }
}
