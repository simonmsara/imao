<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    public function update()
    {
        auth()->user()->update($this->validator());
    }

    public function destroy()
    {
        User::destroy(auth()->id());
        auth()->logout();
    }

    protected function validator()
    {
        return request()->validate([
            'email' => 'email',
            'firstname' => 'regex:/^[A-Za-z\s-_]+$/',
            'lastname' => 'regex:/^[A-Za-z\s-_]+$/',
        ]);
    }
}
