<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favourite;

class FavouriteController extends Controller
{
    public function store(){
        $validated = $this->validateFavourite();
        $newFavourite = new Favourite;
        $newFavourite->favourite($validated);
    }

    public function destroy() {
        $validated = $this->validateFavourite();
        $favourite = Favourite::where([
            'user_id' => auth()->id(),
            'post_id' => $validated['post_id']
        ])->first();
        $this->authorize('delete', $favourite);
        $favourite->delete();
    }

    protected function validateFavourite() {
        return request()->validate(['post_id' => 'required|integer']);
    }
}
