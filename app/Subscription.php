<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $guarded= [];

    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function subscribe($validatedRequest) {
        Subscription::create([
            'user_id' => auth()->id(),
            'subscribable_id' => $validatedRequest['subscribable_id'],
            'subscribable_type' => $validatedRequest['subscribable_type']
        ]);
    }
}
