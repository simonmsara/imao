<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Request;

class Favourite extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function favourite($validated) {
        if($this->notYetFavourited($validated)) {
            Favourite::create([
                'user_id' => auth()->id(),
                'post_id' => $validated['post_id']
           ]);
        }
    }

    protected function notYetFavourited($validated) {
        if($this->where([
            'user_id' => auth()->id(),
            'post_id' => $validated['post_id']
        ])->exists()) {
            return false;
        }
        return true;
    }

    
}
