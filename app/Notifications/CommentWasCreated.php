<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class CommentWasCreated extends Notification
{
    use Queueable;

    public $comment;
    public $subject;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($comment)
    {
        $this->comment = $comment;
        $this->subject = 'commented';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    { 
        return [
            'link_id' => $this->comment->commentable_id,
            'link_type' => $this->comment->commentable_type,
            'notification_subject' => $this->subject,
            'notification_body' => $this->comment->body,
            'notifier' => $this->comment->user
        ];
    }

    public function toBroadcast($notifiable)
    { 
        return new BroadcastMessage([
            'link_id' => $this->comment->commentable_id,
            'link_type' => $this->comment->commentable_type,
            'notification_subject' => $this->subject,
            'notification_body' => $this->comment->body,
            'notifier_id' => $this->comment->user_id,
            'notifier_name' => $this->comment->user->firstname
        ]);
    }
}
